#include <iostream>
#include <string>

class Cat {
public:
    explicit Cat();
    ~Cat();

    void meow() const;
};

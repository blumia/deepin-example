#include "cat.h"

#include "makenoise.h"

Cat::Cat()
{

}

Cat::~Cat()
{

}

void Cat::meow() const
{
    std::cout << make_noise(Animal::Cat) << std::endl;
}

/*
 * Copyright (C) 2021 Example Technology Co., Ltd.
 *
 * Author:     Jone Doe <jonedoe@example.com>
 *
 * Maintainer: John Doe <johndoe@example.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <string>

enum Animal {
    Cat,
    Dog,
    Bird,
    Fish,
};

inline std::string make_noise(const enum Animal & animal) {
    switch (animal) {
        case Cat:
            return "meowwww";
        default:
            throw "Not Implemented";
    }
}
